import { UserModel } from './UserModel';

export class PostModel {

  _id?: string;

  title: string;

  content: string;

  authorId: string;

  author?: Array<Partial<UserModel>>;

  created: string;

  slug: string;

}
